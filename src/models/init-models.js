var DataTypes = require("sequelize").DataTypes;
var _todo = require("./todo");

function initModels(sequelize) {
  var todo = _todo(sequelize, DataTypes);


  return {
    todo,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
