const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('todo', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    status: {
      type: DataTypes.ENUM('pending', 'completed'),
      allowNull: false,
      validate: {
        notNull: {
          msg: "The `status` field is missing"
        },
        isIn: {
          args: [['pending', 'completed']],
          msg: "`status` must be either pending or completed"
        }
      }
    },
    message: {
      type: DataTypes.STRING(255),
      allowNull: false,
      validate: {
        notNull: {
          msg: "The `message` field is missing"
        },
        len: {
          args: [0, 255],
          msg: "The `message` field is too long"
        }
      },
    },
    due_date: {
      type: DataTypes.DATE,
      allowNull: true,
      validate: {
        isDate: {
          args: true,
          msg: "`due_date` must be have date format"
        }
      }
    }
  }, {
    sequelize,
    tableName: 'todo',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
