const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const cors = require('@koa/cors');
const logger = require('koa-morgan')
const apis = require('./routes');
require('dotenv').config();
const errorHandler = require('./errorHandler');

const app = new Koa();
const port = process.env.PORT || 3000;

app
  .use(cors())
  .use(logger('common'))
  .use(bodyParser())
  .use(errorHandler)
  .use(apis.routes())
  .use(apis.allowedMethods())
  .listen(port, () => {
    console.log(`Server running on port ${port}`);
  });