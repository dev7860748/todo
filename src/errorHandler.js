const Sequelize = require('sequelize');

async function errorHandler(ctx, next) {
  try {
    await next();
  } catch (error) {
    if (error instanceof Sequelize.ValidationError) {
      ctx.status = 400;
      ctx.body = { error: 'Validation error', details: error.errors.map(e => e.message) };
    } else if (error.status) {
      ctx.status = error.status;
      ctx.body = { error: error.message };
    } else {
      ctx.status = 500;
      ctx.body = { error: 'Internal Server Error', details: error };
      console.log(ctx.body)
    }

    // Log the error
    console.error('Custom error handling middleware:', error);
  }
}

module.exports = errorHandler;