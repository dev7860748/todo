const Router = require('koa-router');
const fs = require('fs');

const router = new Router();

const path = require('path');
const basename = path.basename(__filename);

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (
      file.indexOf('.') !== 0 &&
      file !== basename &&
      file.slice(-3) === '.js' &&
      file.indexOf('.test.js') === -1
    );
  })
  .forEach(file => {
    const routes = require(path.join(__dirname, file));
    router.use('', routes.routes());
  });

module.exports = router;