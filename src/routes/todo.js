const Router = require('koa-router');
const models = require('../models');

const router = new Router();

router.post('/todo', async (ctx) => {
  await models.todo.create(ctx.request.body)
    .then((todo) => {
      ctx.status = 201;
      ctx.body = todo;
    });
});

router.get('/todo', async (ctx) => {
  await models.todo.findAll()
    .then((todos) => {
      ctx.status = 200;
      ctx.body = todos;
    });
});

router.get('/todo/:id', async (ctx) => {
  const id = ctx.request.params.id;

  await models.todo.findByPk(id)
    .then((todo) => {
      if (!todo) {
        throw ({ message: `Todo with id ${id} does not exist`, status: 404 })
      }

      ctx.status = 200;
      ctx.body = todo;
    });
});

router.patch('/todo/:id', async (ctx) => {
  const id = ctx.request.params.id;
  const body = ctx.request.body;

  const todo = await models.todo.findByPk(id);

  if (!todo) {
    throw ({ message: `Todo with id ${id} does not exist`, status: 404 })
  }

  await models.todo.update(body, {
    where: { id: id },
  })
    .then(async () => {
      const todo = await models.todo.findByPk(id);

      ctx.status = 200;
      ctx.body = todo;
    });
});

router.delete('/todo/:id', async (ctx) => {
  const id = ctx.request.params.id;

  await models.todo.destroy({
    where: {
      id: id
    }
  })
    .then((res) => {
      if (res === 0) {
        throw ({ message: `Todo with id ${id} does not exist`, status: 404 });
      }

      ctx.status = 200;
      ctx.body = { message: `Todo with id ${id} has been successfully deleted` };
    });
});

module.exports = router;