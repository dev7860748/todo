# TODO

This project was generated with NodeJS version 16.14.0

## Database

You can find the database file in the project root `todo_db.sql`.

## Development server

To run the server follow these steps:
1) Import the db to your mysql server
2) Check the `config.json` file which has the database configurations. Adjust the config according to your db server
3) In the project root directory, run `npm install` to install the project dependencies
4) In the project root directory, run `npm start` for the server to start

After that, you can start sending requests to port 3000 (`http://localhost:3000/`)

### File Structure
```
├── todo/									   
│   ├── src/
│   │   ├── config/
│   │   │   ├── config.json
│   │   ├── models/
│   │   │   ├── index.js
│   │   │   ├── init-model.js
│   │   │   ├── todo.js
│   │   ├── routes/
│   │   │   ├── index.js
│   │   │   ├── todo.js
│   │   ├── .env
│   │   ├── app.js
│   │   ├── errorHandler.js
│   ├── .gitignore
│   ├── package.json
│   ├── README.md								   

```